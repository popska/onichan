(define-module (oniichan packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages aidc)
  #:use-module (gnu packages base)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages xdisorg)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public st
  (package
   (inherit (specification->package "st"))
   (name "my-st")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/popska/st")
                  (commit "6dc8c205305d0c9a5e7543c00e138bde0e6b2ec1")))
            (sha256
             (base32 "000jx82q0m0flw91ksldkqsp5fq62y1k5gwx4d1spmpgvmavm33f"))))))

(define-public password-store
  (package
   (name "my-password-store")
   (version "1.7.4")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/popska/password-store")
                  (commit "abb24cbd172331185347dfaebd2bb61f1bf553bd")))
            (sha256
             (base32
              "1qkr74y2slva7srqhf1if2xdrc4784vclx657z0d5a3f2lld0m37"))
            ;; TODO: is this OK?
            ;;(patches (search-patches "password-store-tree-compat.patch"))
            (file-name (git-file-name name version))))
   (build-system gnu-build-system)
   (arguments
    '(#:phases
      (modify-phases %standard-phases
                     (delete 'configure)
                     (delete 'build)
                     (add-before 'install 'patch-system-extension-dir
                                 (lambda* (#:key outputs #:allow-other-keys)
                                   (let* ((out (assoc-ref outputs "out"))
                                          (extension-dir (string-append out "/lib/password-store/extensions")))
                                     (substitute* "src/password-store.sh"
                                                  (("^SYSTEM_EXTENSION_DIR=.*$")
                                                   ;; lead with whitespace to prevent 'make install' from
                                                   ;; overwriting it again
                                                   (string-append " SYSTEM_EXTENSION_DIR=\""
                                                                  "${PASSWORD_STORE_SYSTEM_EXTENSION_DIR:-"
                                                                  extension-dir
                                                                  "}\"\n"))))))
                     (add-before 'install 'patch-program-name
                                 ;; Use pass not .pass-real in tmpdir and cmd_usage
                                 (lambda _
                                   (substitute* "src/password-store.sh"
                                                (("^PROGRAM=.*$")
                                                 "PROGRAM=\"pass\"\n"))))
                     (add-before 'install 'patch-passmenu-path
                                 ;; FIXME Wayland support requires ydotool and dmenu-wl packages
                                 ;; We are ignoring part of the script that gets executed if
                                 ;; WAYLAND_DISPLAY env variable is set, leaving dmenu-wl and ydotool
                                 ;; commands as is.
                                 (lambda* (#:key inputs #:allow-other-keys)
                                   (substitute* "contrib/dmenu/passmenu"
                                                (("dmenu=dmenu\n")
                                                 (string-append "dmenu="
                                                                (search-input-file inputs "/bin/dmenu")
                                                                "\n"))
                                                (("xdotool=\"xdotool")
                                                 (string-append "xdotool=\""
                                                                (search-input-file inputs "/bin/xdotool"))))))
                     (add-after 'install 'install-passmenu
                                (lambda* (#:key outputs #:allow-other-keys)
                                  (let* ((out (assoc-ref outputs "out"))
                                         (bin (string-append out "/bin")))
                                    (install-file "contrib/dmenu/passmenu" bin))))
                     (add-after 'install 'install-phrases
                                (lambda* (#:key outputs #:allow-other-keys)
                                  (let* ((out (assoc-ref outputs "out"))
                                         (dict (string-append out "/share/dict")))
                                    (copy-recursively "src/phrases" dict))))
                     (add-before 'install 'patch-phrases-dir
                                 (lambda* (#:key outputs #:allow-other-keys)
                                   (let* ((out (assoc-ref outputs "out"))
                                          (dict (string-append out "/share/dict")))
                                     (substitute* "src/password-store.sh"
                                                  (("\\$\\(dirname \\$\\(realpath \\$0\\)\\)/phrases/eff_large_wordlist.txt")
                                                   (string-append dict
                                                                  "/eff_large_wordlist.txt"))))))
                     (add-after 'install 'wrap-path
                                (lambda* (#:key inputs outputs #:allow-other-keys)
                                  (let* ((out (assoc-ref outputs "out"))
                                         (requisites '("getopt" "git" "gpg" "qrencode" "sed"
                                                       "tree" "which" "wl-copy" "xclip"))
                                         (path (map (lambda (pkg)
                                                      (dirname (search-input-file
                                                                inputs (string-append "/bin/" pkg))))
                                                    requisites)))
                                    (wrap-program (string-append out "/bin/pass")
                                                  `("PATH" ":" prefix (,(string-join path ":"))))))))
      #:make-flags (list "CC=gcc" (string-append "PREFIX=" %output)
                         "WITH_ALLCOMP=yes"
                         (string-append "BASHCOMPDIR="
                                        %output "/etc/bash_completion.d"))
      ;; Parallel tests may cause a race condition leading to a
      ;; timeout in some circumstances.
      #:parallel-tests? #f
      #:test-target "test"))
   (native-search-paths
    (list (search-path-specification
           (variable "PASSWORD_STORE_SYSTEM_EXTENSION_DIR")
           (separator #f)             ;single entry
           (files '("lib/password-store/extensions")))))
   (inputs
    (list dmenu
          util-linux
          git
          gnupg
          qrencode
          sed
          tree
          which
          wl-clipboard
          xclip
          xdotool))
   (home-page "https://www.passwordstore.org/")
   (synopsis "Encrypted password manager")
   (description "Password-store is a password manager which uses GnuPG to
store and retrieve passwords.  The tool stores each password in its own
GnuPG-encrypted file, allowing the program to be simple yet secure.
Synchronization is possible using the integrated git support, which commits
changes to your password database to a git repository that can be managed
through the pass command.")
   (license license:gpl2+)))
